package com.aiocdawacs.smart.pharmacy.interceptor;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.grpc.CallOptions;
import io.grpc.Channel;
import io.grpc.ClientCall;
import io.grpc.ClientInterceptor;
import io.grpc.MethodDescriptor;

public class LogGrpcInterceptor implements ClientInterceptor {

	private static final Logger log = LoggerFactory.getLogger(LogGrpcInterceptor.class);

	@Override
	public <ReqT, RespT> ClientCall<ReqT, RespT> interceptCall(MethodDescriptor<ReqT, RespT> method,
			CallOptions callOptions, Channel next) {
		log.info(ToStringBuilder.reflectionToString(method, ToStringStyle.NO_CLASS_NAME_STYLE));
		log.info(ToStringBuilder.reflectionToString(callOptions, ToStringStyle.NO_CLASS_NAME_STYLE));
		log.info(ToStringBuilder.reflectionToString(next, ToStringStyle.NO_CLASS_NAME_STYLE));
		return next.newCall(method, callOptions);
	}

}