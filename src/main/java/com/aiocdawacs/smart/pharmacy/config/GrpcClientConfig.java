package com.aiocdawacs.smart.pharmacy.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.aiocdawacs.smart.pharmacy.interceptor.LogGrpcInterceptor;

import io.grpc.ClientInterceptor;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import net.devh.boot.grpc.client.config.GrpcChannelProperties;
import net.devh.boot.grpc.client.interceptor.GrpcGlobalClientInterceptor;

@Configuration
@Import(GrpcChannelProperties.class)
public class GrpcClientConfig {

	@GrpcGlobalClientInterceptor
	ClientInterceptor logClientInterceptor() {
		return new LogGrpcInterceptor();
	}

	//	@Bean
	//	public ManagedChannel managedChannel(@Value("${spring.cloud.consul.host:127.0.0.1}") String host,
	//			@Value("${spring.cloud.consul.port:9345}") int port,
	//			@Value("${connection.idle-timeout}") int timeout,
	//			@Value("${connection.max-inbound-message-size}") int maxInBoundMessageSize,
	//			@Value("${connection.max-inbound-metadata-size}") int maxInBoundMetadataSize,
	//			@Value("${connection.load-balancing-policy}") String loadBalancingPolicy,
	//			@Qualifier("userResolver") NameResolverProvider nameResolverProvider) {
	//		return ManagedChannelBuilder
	//				.forTarget("consul://" + host + ":" + port)                     // build channel to server with server's address
	//				.keepAliveWithoutCalls(false)                                   // Close channel when client has already received response
	//				.idleTimeout(timeout, TimeUnit.MILLISECONDS)                    // 10000 milliseconds / 1000 = 10 seconds --> request time-out
	//				.maxInboundMetadataSize(maxInBoundMetadataSize * 1024 * 1024)   // 2KB * 1024 = 2MB --> max message header size
	//				.maxInboundMessageSize(maxInBoundMessageSize * 1024 * 1024)     // 10KB * 1024 = 10MB --> max message size to transfer together
	//				.defaultLoadBalancingPolicy(loadBalancingPolicy)                // set load balancing policy for channel
	//				.nameResolverFactory(nameResolverProvider)                      // using Consul service discovery for DNS querying
	//				/* .intercept(clientInterceptor) */                                   // add internal credential authentication
	//				.usePlaintext()                                                 // use plain-text to communicate internally
	//				.build();                                                       // Build channel to communicate over gRPC
	//	}

	@Bean
	public ManagedChannel managedChannel(
			@Value("${spring.cloud.consul.host:/127.0.0.1}") String host,
			@Value("${spring.cloud.consul.port:9345}") int port) {
		return ManagedChannelBuilder.forTarget("dns://" + host + ":" + port).keepAliveWithoutCalls(false).usePlaintext().build();                                                       // Build channel to communicate over gRPC
	}
}
