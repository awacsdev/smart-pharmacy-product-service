package com.aiocdawacs.smart.pharmacy.config;

import java.util.Collections;

import javax.servlet.http.HttpServletRequest;

import org.springdoc.core.SpringDocUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.tags.Tag;

@OpenAPIDefinition(servers = { 
		@Server(url = "https://app.awacscloud.tech/productservice/"),	// live staging gcp bastion-1
        @Server(url = "https://qa.awacscloud.tech/productservice/"),	// live qa docker bastion-1
        @Server(url = "http://productservice.awacscloud.com/"),	//docker
		@Server(url = "http://localhost:8181/"),		// development
		@Server(url = "https://192.168.99.100/productservice/")	//dummy production k8s
}) 	
@Configuration
public class OpenApiConfig {

    private static final String API_KEY = "apiKey";
	
	@Bean
	public OpenAPI customOpenAPI1(@Value("${springdoc.version:1.x.x}") String appVersion) {
		SpringDocUtils.getConfig().removeRequestWrapperToIgnore(HttpServletRequest.class);

		return new OpenAPI()
				.components(new Components())
				.info(new Info().title("Pharmacy Data Application API").description(
						"AWACS Cloud"))
				.info(new Info().title("AWACS Pharmacy API")
						.description("Data application")
						.version(appVersion)
						.license(new License().name("Apache 2.0").url("http://springdoc.org")))
				.externalDocs(new ExternalDocumentation()
						.description("Awacs cloud wiki documentation")
						.url("https://awacscloud.wiki.github.org/docs"))
				.components(new Components().addSecuritySchemes(API_KEY,apiKeySecuritySchema())) // define the apiKey SecuritySchema
                .security(Collections.singletonList(new SecurityRequirement().addList(API_KEY)))
				.addTagsItem(new Tag().name("aiocd pharmasofttech awacs pvt ltd".toUpperCase()));
		
	}
	
    public SecurityScheme apiKeySecuritySchema() {
        return new SecurityScheme()
                .name("Authorization")
                .description("jwt access token for service".toUpperCase())
                .in(SecurityScheme.In.HEADER)
                .type(SecurityScheme.Type.APIKEY);
    }
}
