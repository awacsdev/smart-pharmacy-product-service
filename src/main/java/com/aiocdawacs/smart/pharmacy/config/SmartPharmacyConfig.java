
package com.aiocdawacs.smart.pharmacy.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.aiocdawacs.files.config.FileStorageConfig;

@Import(FileStorageConfig.class)
@Configuration
public class SmartPharmacyConfig extends WebSecurityConfigurerAdapter{

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		
		httpSecurity
		.authorizeRequests().antMatchers("/api/**").permitAll().and()
		.authorizeRequests().antMatchers("/actuator/**").permitAll().and()
		.authorizeRequests().antMatchers("/h2-console/**").permitAll().and()
		.authorizeRequests().antMatchers("/swagger-ui/**").permitAll().and()
		.authorizeRequests().antMatchers("/v3/api-docs").permitAll().and()
		.authorizeRequests().antMatchers("/**").permitAll();

		httpSecurity.csrf().disable();
		httpSecurity.headers().frameOptions().disable();
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers(HttpMethod.GET, "/swagger-ui/**");
		super.configure(web);
	}
}
