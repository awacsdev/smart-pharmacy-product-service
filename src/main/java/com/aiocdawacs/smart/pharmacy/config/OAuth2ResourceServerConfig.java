package com.aiocdawacs.smart.pharmacy.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import com.aiocdawacs.smart.pharmacy.service.RemoteGrpcTokenService;

@EnableResourceServer
@Import(SecurityProperties.class)
@Configuration
public class OAuth2ResourceServerConfig extends ResourceServerConfigurerAdapter {

	private static final String ROOT_PATTERN = "/api/**";
	@Value("${awacs.cloud.auth-server.check-token:http://localhost:8100/oauth/check_token}")
	private String checkTokenURI;

	private TokenStore tokenStore;
	
	@Autowired
	RemoteGrpcTokenService remoteGrpcTokenService;

	@Bean
	public PasswordEncoder oauthClientPasswordEncoder() {
		return new BCryptPasswordEncoder(4);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {

		http.formLogin().disable();
		http.headers().frameOptions().disable();

		http.authorizeRequests()
		.antMatchers(HttpMethod.GET, ROOT_PATTERN).access("#oauth2.hasScope('read')")
		.antMatchers(HttpMethod.POST, ROOT_PATTERN).access("#oauth2.hasScope('write')")
		.antMatchers(HttpMethod.PATCH, ROOT_PATTERN).access("#oauth2.hasScope('write')")
		.antMatchers(HttpMethod.PUT, ROOT_PATTERN).access("#oauth2.hasScope('write')")
		.antMatchers(HttpMethod.DELETE, ROOT_PATTERN).access("#oauth2.hasScope('write')");

		http
		.authorizeRequests()
		.antMatchers(ROOT_PATTERN).hasAuthority("USER").and()
		.authorizeRequests().antMatchers("/api/**").hasAuthority("USER")
		.antMatchers("/login", "/error", "/log").permitAll()
		.and()
		.authorizeRequests().antMatchers("/h2-console/**").permitAll()
		.and()
		.authorizeRequests().antMatchers("/swagger-ui/**").permitAll()
		.and()
		.authorizeRequests().antMatchers("/actuator/**").permitAll()
		.and()
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

	}


	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		resources.tokenServices(remoteGrpcTokenService);
		resources.resourceId("resource-server-rest-api");
	}

	@Bean
	public TokenStore tokenStore() {
		if (tokenStore == null) {
			tokenStore = new JwtTokenStore(jwtAccessTokenConverter());
		}
			return tokenStore;
	}

	@Bean
	public JwtAccessTokenConverter jwtAccessTokenConverter() {
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
		return converter;
	}
}