
package com.aiocdawacs.smart.pharmacy.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aiocdawacs.cloud.stream.model.CloudEvent;
import com.aiocdawacs.cloud.stream.model.CloudEventType;
import com.aiocdawacs.cloud.stream.service.AwacsCloudEventProviderEnum;
import com.aiocdawacs.cloud.stream.service.CloudEventBuilder;
import com.aiocdawacs.cloud.stream.service.CloudEventNameConstants;
import com.aiocdawacs.cloud.stream.service.CloudEventPublisherService;
import com.aiocdawacs.smart.pharmacy.SmartPharmacyProductServiceApplication;
import com.aiocdawacs.smart.pharmacy.model.Pharmasist;
import com.aiocdawacs.smart.pharmacy.repository.PharmasistRepository;

@RestController
@RequestMapping("/api/pharmasist")
public class PharmasistController {

	@Autowired
	PharmasistRepository pharmasistRepository;

	@Autowired
	CloudEventPublisherService cloudEventPublisherService;

	
	@GetMapping("/all")
	public List<Pharmasist> getAllPharmasists(OAuth2Authentication auth,  @RequestHeader HttpHeaders headers, HttpSession session, HttpServletRequest request){
		List<Pharmasist> result = (List<Pharmasist>)this.pharmasistRepository.findAll();
		
		CloudEvent getAllPharmasistsEvent = new CloudEventBuilder()
				.withName(CloudEventNameConstants.PHARMASIST_GETALL_EVENT_NAME)
				.withSource(SmartPharmacyProductServiceApplication.SERVICE_NAME)
				.withType(CloudEventType.GET_ALL_PHARMASISTS_DETAILS)
				.withSessionId(session.getId())
				.withUserId(auth.getOAuth2Request().getClientId())
				.withAuthToken(headers.get("Authorization").toString())
				.withSessionCreatedAt(String.valueOf(session.getCreationTime()))
				.withAccessURL(request.getRequestURI())
				.build();

		cloudEventPublisherService.publishMessage(getAllPharmasistsEvent, AwacsCloudEventProviderEnum.GoogleCloudPlatformPubSub);

		return result;
	}

}
