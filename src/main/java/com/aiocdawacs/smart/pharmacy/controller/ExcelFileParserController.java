package com.aiocdawacs.smart.pharmacy.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.aiocdawacs.files.excel.DistributorFormatExcelHelper;
import com.aiocdawacs.files.excel.DistributorFormatPojo;
import com.aiocdawacs.files.service.StorageFileNotFoundException;
import com.aiocdawacs.files.service.StorageService;

@RestController
@RequestMapping("/api/files/")
public class ExcelFileParserController {

	private final StorageService storageService;

	@Autowired
	public ExcelFileParserController(StorageService storageService) {
		super();
		this.storageService = storageService;
	}

	@PostMapping(path = "/excel/tenant/{tenantId}/distributor/{xlsxFileNameUploaded}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody ResponseEntity<List<DistributorFormatPojo>> processExcel(@PathVariable String tenantId, @PathVariable String xlsxFileNameUploaded) throws StorageFileNotFoundException {
		Resource file = storageService.loadAsResource(xlsxFileNameUploaded);
		List<DistributorFormatPojo> products  = DistributorFormatExcelHelper.excelToDistributors(file);
		return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(products);
	}
}