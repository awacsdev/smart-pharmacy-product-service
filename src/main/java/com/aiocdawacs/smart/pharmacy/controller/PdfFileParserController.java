package com.aiocdawacs.smart.pharmacy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aiocdawacs.files.pdf.PDFTableExtractor;
import com.aiocdawacs.files.pdf.entity.Table;
import com.aiocdawacs.files.service.StorageFileNotFoundException;
import com.aiocdawacs.files.service.StorageService;


@RestController
@RequestMapping("/api/files/")
public class PdfFileParserController {

	private final StorageService storageService;

	@Autowired
	public PdfFileParserController(StorageService storageService) {
		super();
		this.storageService = storageService;
	}
	
	@GetMapping(path = "/pdf/tenant/{tenantId}/{pdfFileNameUploaded}", produces = {"text/csv","text/html"})
	public ResponseEntity<String> processPDF(@PathVariable String tenantId, @PathVariable String pdfFileNameUploaded) throws StorageFileNotFoundException {

		Resource file = storageService.loadAsResource(pdfFileNameUploaded);

		PDFTableExtractor extractor =  new PDFTableExtractor();

		List<Table> tables = extractor.setSource(file)
				.addPage(1)
				.exceptLine(new int [0])
				.extract();

		//String html = tables.get(0).toHtml();		// 
		String csv  = tables.get(0).toString();

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + pdfFileNameUploaded + ".csv")
				.contentLength(csv.length())
				.contentType(MediaType.parseMediaType("text/csv"))		// accept header ??
				.body(csv);

	}

	
	@ExceptionHandler(StorageFileNotFoundException.class)
	public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
		return ResponseEntity.notFound().build();
	}
}
