package com.aiocdawacs.smart.pharmacy.controller;


import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import com.aiocdawacs.awacscommonsqueries.model.ProductOrderDto;
import com.aiocdawacs.awacscommonsqueries.service.DatabaseQueryService;
import com.aiocdawacs.files.service.StorageFileNotFoundException;
import com.aiocdawacs.files.service.StorageService;

@RestController
@RequestMapping("/api/files/")
public class FileUploadController {

	private final DatabaseQueryService awacsQueryService;

	private final StorageService storageService;

	@Autowired
	public FileUploadController(StorageService storageService, DatabaseQueryService awacsQueryService) {
		super();
		this.storageService = storageService;
		this.awacsQueryService = awacsQueryService;
	}

	@GetMapping("/")
	public String listUploadedFiles(Model model) throws IOException {

		model.addAttribute("files", storageService.loadAll().map(
				path -> MvcUriComponentsBuilder.fromMethodName(FileUploadController.class,
						"serveFile", path.getFileName().toString()).build().toUri().toString())
				.collect(Collectors.toList()));

		return "uploadForm";
	}

	@GetMapping("/listFiles")
	public @ResponseBody ResponseEntity<List<String>> listAllFiles() throws IOException {
		return ResponseEntity.ok(storageService.loadAll()
				.map(path -> MvcUriComponentsBuilder
						.fromMethodName(FileUploadController.class, "serveFile", path.getFileName().toString()).build()
						.toUri().toString())
				.collect(Collectors.toList()));
	}


	@GetMapping("/{filename:.+}")
	public @ResponseBody ResponseEntity<Resource> serveFile(@PathVariable String filename) throws StorageFileNotFoundException {
		Resource file = storageService.loadAsResource(filename);
		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
	}

	@PostMapping("/")
	public String handleFileUpload(@RequestParam("file") MultipartFile file) {
		storageService.store(file);
		return "File successfully uploaded ("+file.getOriginalFilename()+"!)";
	}


	@GetMapping("/fetchBulletOrder")
	public Optional<List<ProductOrderDto>> fetchBulletOrder(@RequestParam(name = "pharmasistCode") String pharmasistCode){
		return Optional.of(awacsQueryService.getByBulletOrderSearchQuery(pharmasistCode));
	}

}