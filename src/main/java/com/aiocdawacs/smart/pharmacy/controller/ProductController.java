package com.aiocdawacs.smart.pharmacy.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aiocdawacs.cloud.stream.model.CloudEvent;
import com.aiocdawacs.cloud.stream.model.CloudEventType;
import com.aiocdawacs.cloud.stream.service.AwacsCloudEventProviderEnum;
import com.aiocdawacs.cloud.stream.service.CloudEventBuilder;
import com.aiocdawacs.cloud.stream.service.CloudEventNameConstants;
import com.aiocdawacs.cloud.stream.service.CloudEventPublisherService;
import com.aiocdawacs.smart.pharmacy.SmartPharmacyProductServiceApplication;
import com.aiocdawacs.smart.pharmacy.model.Product;
import com.aiocdawacs.smart.pharmacy.repository.ProductRepository;

@RestController
@RequestMapping("/api/product")
public class ProductController {

	@Autowired
	ProductRepository productRepository;

	@Autowired
	CloudEventPublisherService cloudEventPublisherService;

	@GetMapping( path = "/all")
	public List<Product> getAllProducts(OAuth2Authentication auth,  @RequestHeader HttpHeaders headers, HttpSession session, HttpServletRequest request){

		List<Product> result = productRepository.findAll();

		CloudEvent getAllProductEvent = new CloudEventBuilder()
				.withName(CloudEventNameConstants.PRODUCT_DETAILS_GETALL_EVENT_NAME)
				.withSource(SmartPharmacyProductServiceApplication.SERVICE_NAME)
				.withType(CloudEventType.GET_ALL_PRODUCT_DETAILS)
				.withSessionId(session.getId())
				.withUserId(auth.getOAuth2Request().getClientId())
				.withAuthToken(headers.get("Authorization").toString())
				.withAccessURL(request.getRequestURI())
				.withSessionCreatedAt(String.valueOf(session.getCreationTime()))
				.build();

		cloudEventPublisherService.publishMessage(getAllProductEvent, AwacsCloudEventProviderEnum.GoogleCloudPlatformPubSub);

		return result;
	}

	@GetMapping( path = "/add")
	public void saveProduct(OAuth2Authentication auth,  @RequestHeader HttpHeaders headers, HttpSession session, HttpServletRequest request, @RequestBody Product productToSave){
		productRepository.save(productToSave);

		CloudEvent makeProductEvent = new CloudEventBuilder()
				.withName(CloudEventNameConstants.PRODUCT_SAVE_EVENT_NAME)
				.withSource(SmartPharmacyProductServiceApplication.SERVICE_NAME)
				.withType(CloudEventType.CREATE_PRODUCT)
				.withSessionId(session.getId())
				.withUserId(auth.getOAuth2Request().getClientId())
				.withAuthToken(headers.get("Authorization").toString())
				.withAccessURL(request.getRequestURI())
				.withSessionCreatedAt(String.valueOf(session.getCreationTime()))
				.build();

		cloudEventPublisherService.publishMessage(makeProductEvent, AwacsCloudEventProviderEnum.GoogleCloudPlatformPubSub);

	}

	@GetMapping( path = "/{id}")
	public Product getProduct(OAuth2Authentication auth,  @RequestHeader HttpHeaders headers, HttpSession session, HttpServletRequest request, @PathVariable Long id){

		CloudEvent getProductByIdEvent = new CloudEventBuilder()
				.withName(CloudEventNameConstants.PRODUCT_GET_BY_ID_EVENT_NAME)
				.withSource(SmartPharmacyProductServiceApplication.SERVICE_NAME)
				.withType(CloudEventType.PRODUCT_GET_BY_PRODUCT_ID)
				.withInfo("productId: "+id)
				.withAccessURL(request.getRequestURI())
				.withSessionId(session.getId())
				.withUserId(auth.getOAuth2Request().getClientId())
				.withAuthToken(headers.get("Authorization").toString())
				.withSessionCreatedAt(String.valueOf(session.getCreationTime()))
				.build();

		cloudEventPublisherService.publishMessage(getProductByIdEvent, AwacsCloudEventProviderEnum.GoogleCloudPlatformPubSub);

		return productRepository.findById(id).orElse(null);
	}
}
