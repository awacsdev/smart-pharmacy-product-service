package com.aiocdawacs.smart.pharmacy.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aiocdawacs.cloud.stream.model.CloudEvent;
import com.aiocdawacs.cloud.stream.model.CloudEventType;
import com.aiocdawacs.cloud.stream.service.AwacsCloudEventProviderEnum;
import com.aiocdawacs.cloud.stream.service.CloudEventBuilder;
import com.aiocdawacs.cloud.stream.service.CloudEventNameConstants;
import com.aiocdawacs.cloud.stream.service.CloudEventPublisherService;
import com.aiocdawacs.smart.pharmacy.SmartPharmacyProductServiceApplication;
import com.aiocdawacs.smart.pharmacy.model.Distributor;
import com.aiocdawacs.smart.pharmacy.repository.DistributorRepository;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@RestController
@RequestMapping("/api/distributor")
public class DistributorController {

	@Autowired
	DistributorRepository distributorRepository;

	@Autowired
	CloudEventPublisherService cloudEventPublisherService;


	@GetMapping("/all")
	@ApiResponses(value = {@ApiResponse(responseCode = "200", content = @Content(schema = @Schema(hidden = true))) })
	public List<Distributor> getAllDistributors(OAuth2Authentication auth,  @RequestHeader HttpHeaders headers, HttpSession session, HttpServletRequest request	){
		List<Distributor> result = (List<Distributor>) this.distributorRepository.findAll();
		
		CloudEvent getAllDistributorsEvent = new CloudEventBuilder()
				.withName(CloudEventNameConstants.DISTRIBUTOR_GETALL_EVENT_NAME)
				.withSource(SmartPharmacyProductServiceApplication.SERVICE_NAME)
				.withType(CloudEventType.GET_ALL_DISTRIBUTOR_DETAILS)
				.withSessionId(session.getId())
				.withUserId(auth.getOAuth2Request().getClientId())
				.withAuthToken(headers.get("Authorization").toString())
				.withSessionCreatedAt(String.valueOf(session.getCreationTime()))
				.withAccessURL(request.getRequestURI())
				.build();

		cloudEventPublisherService.publishMessage(getAllDistributorsEvent, AwacsCloudEventProviderEnum.GoogleCloudPlatformPubSub);

		return result;
	}
}

