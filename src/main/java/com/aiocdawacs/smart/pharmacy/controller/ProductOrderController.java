package com.aiocdawacs.smart.pharmacy.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aiocdawacs.cloud.stream.model.CloudEvent;
import com.aiocdawacs.cloud.stream.model.CloudEventType;
import com.aiocdawacs.cloud.stream.service.AwacsCloudEventProviderEnum;
import com.aiocdawacs.cloud.stream.service.CloudEventBuilder;
import com.aiocdawacs.cloud.stream.service.CloudEventNameConstants;
import com.aiocdawacs.cloud.stream.service.CloudEventPublisherService;
import com.aiocdawacs.smart.pharmacy.SmartPharmacyProductServiceApplication;
import com.aiocdawacs.smart.pharmacy.model.ProductOrder;
import com.aiocdawacs.smart.pharmacy.repository.ProductOrderRepository;

@RestController
@RequestMapping("/api/product/order")

public class ProductOrderController {

	@Autowired
	ProductOrderRepository productOrderRepository;

	@Autowired
	CloudEventPublisherService cloudEventPublisherService;

	@PostMapping("/makeOrder")
	public Optional<ProductOrder> makeOrder(OAuth2Authentication auth,  @RequestHeader HttpHeaders headers, HttpSession session, HttpServletRequest request, @RequestBody ProductOrder o) {
		ProductOrder productOrder = this.productOrderRepository.save(o);

		CloudEvent makeOrderEvent = new CloudEventBuilder()
				.withName(CloudEventNameConstants.PRODUCT_ORDER_NEW_EVENT_NAME)
				.withSource(SmartPharmacyProductServiceApplication.SERVICE_NAME)
				.withType(CloudEventType.CREATE_ORDER)
				.withInfo("New Order="+productOrder.getId())
				.withSessionId(session.getId())
				.withAccessURL(request.getRequestURI())
				.withUserId(auth.getOAuth2Request().getClientId())
				.withAuthToken(headers.get("Authorization").toString())
				.withSessionCreatedAt(String.valueOf(session.getCreationTime()))
				.build();

		cloudEventPublisherService.publishMessage(makeOrderEvent, AwacsCloudEventProviderEnum.GoogleCloudPlatformPubSub);

		return Optional.of(productOrder);
	}

	@GetMapping("/{orderId}")
	public ResponseEntity<ProductOrder> getOrder(OAuth2Authentication auth,  @RequestHeader HttpHeaders headers, HttpSession session, HttpServletRequest request, @PathVariable Long orderId) {

		CloudEvent getOrderByIdEvent = new CloudEventBuilder()
				.withName(CloudEventNameConstants.PRODUCT_ORDER_GET_BY_ID)
				.withSource(SmartPharmacyProductServiceApplication.SERVICE_NAME)
				.withType(CloudEventType.GET_ORDER_BY_ORDER_ID)
				.withInfo("order:"+orderId)
				.withAccessURL(request.getRequestURI())
				.withSessionId(session.getId())
				.withUserId(auth.getOAuth2Request().getClientId())
				.withAuthToken(headers.get("Authorization").toString())
				.withSessionCreatedAt(String.valueOf(session.getCreationTime()))
				.build();

		cloudEventPublisherService.publishMessage(getOrderByIdEvent, AwacsCloudEventProviderEnum.GoogleCloudPlatformPubSub);

		return ResponseEntity.of(Optional.ofNullable(this.productOrderRepository.findById(orderId)
				.orElse(null)));
	}

	@GetMapping("/all")
	public List<ProductOrder> getAllOrders(OAuth2Authentication auth,  @RequestHeader HttpHeaders headers, HttpSession session, HttpServletRequest request) {
		List<ProductOrder> result = (List<ProductOrder>) this.productOrderRepository.findAll();

		CloudEvent getOrdersEvent = new CloudEventBuilder()
				.withName(CloudEventNameConstants.PRODUCT_ORDER_GET_ALL)
				.withSource(SmartPharmacyProductServiceApplication.SERVICE_NAME)
				.withType(CloudEventType.GET_ALL_PRODUCT_ORDER_DETAILS)
				.withAccessURL(request.getRequestURI())
				.withSessionId(session.getId())
				.withUserId(auth.getOAuth2Request().getClientId())
				.withAuthToken(headers.get("Authorization").toString())
				.withSessionCreatedAt(String.valueOf(session.getCreationTime()))
				.build();

		cloudEventPublisherService.publishMessage(getOrdersEvent, AwacsCloudEventProviderEnum.GoogleCloudPlatformPubSub);

		return result;
	}
}