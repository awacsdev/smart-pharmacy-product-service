package com.aiocdawacs.smart.pharmacy.service;


import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.stereotype.Service;

import com.aiocdawacs.boot.grpc.lib.CheckTokenReply;
import com.aiocdawacs.boot.grpc.lib.CheckTokenRequest;
import com.aiocdawacs.boot.grpc.lib.GrpcAwacsTokenServiceGrpc;

import io.grpc.Context;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.stub.MetadataUtils;
import io.micrometer.core.instrument.util.StringUtils;

class Constants {

	public static final Metadata.Key<String> AUTHORIZATION_METADATA_KEY = Metadata.Key.of("Authorization", Metadata.ASCII_STRING_MARSHALLER);

	public static final Context.Key<String> CLIENT_ID_CONTEXT_KEY 		= Context.key("clientId");

	private Constants() {
		throw new AssertionError();
	}
}

@Service
public class RemoteGrpcTokenService implements ResourceServerTokenServices {

	Logger logger = LoggerFactory.getLogger(RemoteGrpcTokenService.class);

	enum FrameworkParams {
		client_id, authorities, user_name, aud, jti, scope, active, exp
	}

	//	@GrpcClient("check_token")
	//	private Channel channel;
	//	
	//	@GrpcClient(value = "check_token", interceptors = LogGrpcInterceptor.class)
	//	private GrpcAwacsTokenServiceBlockingStub checkTokenStub;

	private AccessTokenConverter tokenConverter = new DefaultAccessTokenConverter();

	public RemoteGrpcTokenService() {
		super();
	}

	public void setAccessTokenConverter(AccessTokenConverter accessTokenConverter) {
		this.tokenConverter = accessTokenConverter;
	}

	@Override
	public OAuth2Authentication loadAuthentication(String accessToken) throws AuthenticationException, InvalidTokenException {

		if(StringUtils.isEmpty(accessToken)) {
			throw new InvalidTokenException(accessToken);
		}

		ManagedChannel channel = ManagedChannelBuilder.forAddress(System.getProperty("awacs_grpc_host", "localhost"), 9345).usePlaintext().build();

		Metadata basicWakandaAuthHeaderMetadata =new Metadata();
		basicWakandaAuthHeaderMetadata.put(Constants.AUTHORIZATION_METADATA_KEY, getAuthorizationHeader("wakandagrpc", "wakandagrpc"));

		CheckTokenRequest request = CheckTokenRequest.newBuilder().setToken(accessToken).build();

		// BasicToken basicWakandaCredentials = new BasicToken(); CallCredentials has its own hell and heaven so leaving it a backlog. ;)

		CheckTokenReply reply     = MetadataUtils.attachHeaders(GrpcAwacsTokenServiceGrpc.newBlockingStub(channel), basicWakandaAuthHeaderMetadata).checkToken(request);

		if(! reply.getActive()) {
			logger.info(ToStringBuilder.reflectionToString(reply, ToStringStyle.NO_CLASS_NAME_STYLE));
			throw new InvalidTokenException(accessToken);
		}

		ToStringBuilder.reflectionToString((Object)reply, ToStringStyle.JSON_STYLE);

		return tokenConverter.extractAuthentication(toMap(reply));
	}

	private Map<String, ?> toMap(CheckTokenReply reply){
		Map<String, Object> foo = new HashMap<String, Object>();
		foo.put(FrameworkParams.authorities.name(), reply.getAuthoritiesList());
		foo.put(FrameworkParams.client_id.name(), reply.getClientId());
		foo.put(FrameworkParams.aud.name(), "resource-server-rest-api");
		foo.put(FrameworkParams.exp.name(), reply.getExp());
		foo.put(FrameworkParams.jti.name(), UUID.randomUUID().toString());	//FIXME what's that ?
		foo.put(FrameworkParams.scope.name(), reply.getScopeList());
		foo.put(FrameworkParams.user_name.name(), reply.getUserName());
		foo.put("whoami", "wakandagrpc");	// this.clientId

		return foo;
	}

	@Override
	public OAuth2AccessToken readAccessToken(String accessToken) {
		throw new UnsupportedOperationException("Not supported: read access token");
	}

	private String getAuthorizationHeader(String clientId, String clientSecret) {

		if(clientId == null || clientSecret == null) {
			logger.warn("Null Client ID or Client Secret detected. Endpoint that requires authentication will reject request with 401 error.");
		}

		String creds = String.format("%s:%s", clientId, clientSecret);
		try {
			return "Basic " + new String(Base64.getEncoder().encode(creds.getBytes("UTF-8")));
		}
		catch (UnsupportedEncodingException e) {
			throw new IllegalStateException("Could not convert String");
		}
	}
}
